package mogusdk

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"

	"github.com/bitly/go-simplejson"
)

// MoParams is a map string to interface
type MoParams map[string]interface{}

// MoClient holds something about the MoClient
type MoClient struct {
	httpClient http.Client
	baseURL    string
	appKey     string
	appSecret  string
}

// CallBasic will call a mogujie api and return the []bytes
func (cli *MoClient) CallBasic(method string, token string, params *MoParams) (bt []byte, e error) {
	pb := url.Values{}

	pb.Add("app_key", cli.appKey)
	pb.Add("method", method)
	pb.Add("access_token", token)
	pb.Add("format", "json")
	pb.Add("timestamp", fmt.Sprintf("%d", time.Now().Unix()))
	pb.Add("version", "1.0")
	pb.Add("sign_method", "md5")

	for k, v := range *params {
		value := fmt.Sprint(v)
		if k != "" && value != "" {
			pb.Add(k, value)
		}
	}
	mk := make([]string, len(pb))
	i := 0
	for k := range pb {
		mk[i] = k
		i++
	}
	sort.Strings(mk)
	var buffer bytes.Buffer

	for k := range mk {
		key := mk[k]
		buffer.WriteString(key)
		buffer.WriteString(pb.Get(key))
	}

	md5Ctx := md5.New()
	md5Ctx.Write([]byte(cli.appSecret))
	md5Ctx.Write([]byte(buffer.Bytes()))
	md5Ctx.Write([]byte(cli.appSecret))

	cipherStr := md5Ctx.Sum(nil)
	sign := hex.EncodeToString(cipherStr)

	pb.Add("sign", strings.ToUpper(sign))
	requestURL := fmt.Sprintf("http://openapi.mogujie.com/invoke")
	resp, err := cli.httpClient.Get(requestURL + "?" + pb.Encode())
	if err != nil {
		e = err
		return
	}
	defer resp.Body.Close()
	bt, err = ioutil.ReadAll(resp.Body)
	return
}

// Call will call a mogujie api and return an simplejson object
func (cli *MoClient) Call(method string, token string, params *MoParams) (js *simplejson.Json, e error) {
	bytes, err := cli.CallBasic(method, token, params)
	if err != nil {
		return nil, err
	}

	js, err = simplejson.NewJson(bytes)

	if err != nil {
		return nil, err
	}
	return js, err
}

// Init will init a MoClient
func (cli *MoClient) Init(baseURL string, appKey string, appSecret string) {
	cli.baseURL = baseURL
	cli.appKey = appKey
	cli.appSecret = appSecret
	cli.httpClient = http.Client{
		Timeout: time.Duration(10 * time.Second),
	}
}

// Show will show a MoClient info
func (cli *MoClient) Show() {
	fmt.Println("baseURL:", cli.baseURL, "\nappKey:", cli.appKey, "\nappSecret:", cli.appSecret)
}
